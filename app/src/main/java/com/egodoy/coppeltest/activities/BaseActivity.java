package com.egodoy.coppeltest.activities;

import android.Manifest;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.os.PersistableBundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;

/**
 * Created by edgargodoy on 03/09/18.
 */

public abstract class BaseActivity extends AppCompatActivity {


    protected static final int LOCATION_REQUEST_CODE = 1000;
    private static final String TAG = BaseActivity.class.getSimpleName();

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState, @Nullable PersistableBundle persistentState) {
        super.onCreate(savedInstanceState, persistentState);
    }


    protected void requestPermissions(String manifestPermission) {

        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {

            switch (manifestPermission) {

                case Manifest.permission.ACCESS_FINE_LOCATION:

                    if (checkSelfPermission(Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                        requestPermissions(new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, LOCATION_REQUEST_CODE);
                    } else {
                        locationPermissionsGranted(true);
                    }
            }

        }else{

            switch (manifestPermission) {
                case Manifest.permission.ACCESS_FINE_LOCATION:
                    locationPermissionsGranted(true);
                    break;
            }

        }

    }


    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        switch (requestCode ) {

            case LOCATION_REQUEST_CODE : {

                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    locationPermissionsGranted(true);
                } else {
                    locationPermissionsGranted(false);
                }

            }


        }
    }


    protected void locationPermissionsGranted (boolean permissionsGranted){
        Log.d(TAG, "Override method locationPermissionsGranted");
    }



}
