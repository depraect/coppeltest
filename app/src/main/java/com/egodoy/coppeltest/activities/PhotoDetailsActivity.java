package com.egodoy.coppeltest.activities;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.ImageView;

import com.egodoy.coppeltest.R;
import com.egodoy.coppeltest.fragments.MapFragment;
import com.egodoy.coppeltest.models.Photo;
import com.egodoy.coppeltest.presenters.PhotoDetailsPresenter;



import butterknife.BindView;
import butterknife.ButterKnife;

public class PhotoDetailsActivity extends AppCompatActivity implements PhotoDetailsPresenter.PhotoDetailsPresenterCallback {
    private static final String TAG = PhotoDetailsActivity.class.getSimpleName();
    public static final String PHOTO_ID = "PHOTOID";
    PhotoDetailsPresenter presenter;

    @BindView(R.id.image_view) ImageView imageView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_photo_details);
        ButterKnife.bind(this);
        presenter = new PhotoDetailsPresenter(this);
        String photoId = getIntent().getStringExtra(PHOTO_ID);
        presenter.getPhoto(photoId);



    }


    @Override
    public void onGetPhotoSuccess(Photo photo) {
        imageView.setImageBitmap(photo.getImage());

        getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.map_content, MapFragment.newInstance(photo.getLat(), photo.getLng()))
                .commit();

    }

    @Override
    public void onGetPhotoError(Throwable e) {
        Log.d(TAG,"error " + e.getMessage() );
    }
}
