package com.egodoy.coppeltest.activities;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.widget.Toast;

import com.egodoy.coppeltest.R;
import com.egodoy.coppeltest.adapters.PhotoAdapter;
import com.egodoy.coppeltest.application.App;
import com.egodoy.coppeltest.application.Constants;
import com.egodoy.coppeltest.connection.PicassoInterceptor;
import com.egodoy.coppeltest.models.Photo;
import com.egodoy.coppeltest.presenters.PhotosPresenter;
import com.squareup.picasso.OkHttp3Downloader;
import com.squareup.picasso.Picasso;

import java.util.List;
import java.util.Set;

import butterknife.BindView;
import butterknife.ButterKnife;
import okhttp3.OkHttpClient;

public class PhotosActivity extends AppCompatActivity implements
        PhotosPresenter.IPhotoAdapterCallback,
        PhotoAdapter.OnPhotoClickListener {

    private static final String TAG = PhotosActivity.class.getSimpleName();

    @BindView(R.id.recicler_view) RecyclerView recyclerView;

    private PhotosPresenter presenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_photos);
        ButterKnife.bind(this);
        presenter = new PhotosPresenter(this);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));



    }

    @Override
    public void onGetPhotoListSuccess(List<Photo> photos) {
        PhotoAdapter adapter = new PhotoAdapter(photos,this);
        recyclerView.setAdapter(adapter);
    }

    @Override
    public void onGetPhotoListError(Throwable e) {

        Toast.makeText(this,e.getMessage(),Toast.LENGTH_LONG).show();

    }



    @Override
    public void OnPhotoClickListenerTouch(String uri) {
        String[] segemnts = uri.split("/");
        String id = segemnts[segemnts.length-1];
        Intent intent = new Intent(PhotosActivity.this, PhotoDetailsActivity.class);
        intent.putExtra(PhotoDetailsActivity.PHOTO_ID,id);
        startActivity(intent);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        presenter = null;
    }
}
