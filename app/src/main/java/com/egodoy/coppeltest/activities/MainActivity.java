package com.egodoy.coppeltest.activities;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.Build;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;

import com.egodoy.coppeltest.R;
import com.egodoy.coppeltest.connection.MyJsonApi;
import com.egodoy.coppeltest.models.Photo;
import com.egodoy.coppeltest.presenters.MainPresenter;
import com.egodoy.coppeltest.utils.ConvertUtil;
import com.egodoy.coppeltest.utils.CustomLocationProvider;

import java.util.HashSet;
import java.util.Set;

import butterknife.ButterKnife;
import butterknife.OnClick;
import io.reactivex.observers.ResourceObserver;

public class MainActivity extends AppCompatActivity implements MainPresenter.IMainPresenterCallBack {

    private static String TAG = MainActivity.class.getSimpleName();
    private static int REQUEST_PHOTO_CODE = 1001;
    private MainPresenter presenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        presenter = new MainPresenter(this);

    }

    @OnClick(R.id.button_take_photo)
    public void takePhotoButtonClick(){
        Intent intent =  new Intent(MainActivity.this, CameraActivity.class);
        startActivityForResult(intent,REQUEST_PHOTO_CODE);
    }


    @OnClick(R.id.button_view_photo)
    public void viewPhotosButtonClick(){

        startActivity(new Intent(MainActivity.this,PhotosActivity.class));

    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if(resultCode == RESULT_OK && requestCode == REQUEST_PHOTO_CODE){
            Bundle bundle = data.getExtras();
            if(bundle!=null){
                presenter.savePhotoBundle(bundle);
            }

        }
    }

    @Override
    public void onSavePhotoSuccess(Photo photo) {

        Toast.makeText(this,"Se guardo con exito! ",Toast.LENGTH_LONG).show();

    }

    @Override
    public void onSavePhotoError(Throwable e) {

        Toast.makeText(this,"ERROR! " + e.getMessage() ,Toast.LENGTH_LONG).show();

    }
}
