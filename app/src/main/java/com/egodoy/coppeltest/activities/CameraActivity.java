package com.egodoy.coppeltest.activities;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.location.Location;
import android.os.Build;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageButton;

import com.egodoy.coppeltest.R;
import com.egodoy.coppeltest.utils.ConvertUtil;
import com.egodoy.coppeltest.utils.CustomLocationProvider;
import com.wonderkiln.camerakit.CameraKit;
import com.wonderkiln.camerakit.CameraKitError;
import com.wonderkiln.camerakit.CameraKitEvent;
import com.wonderkiln.camerakit.CameraKitEventListener;
import com.wonderkiln.camerakit.CameraKitImage;
import com.wonderkiln.camerakit.CameraKitVideo;
import com.wonderkiln.camerakit.CameraView;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class CameraActivity extends BaseActivity implements CameraKitEventListener {


    public static final String IMAGEN = "IMAGEN";
    public static final String LAT = "LAT";
    public static final String LNG = "LNG";
    private static String TAG = CameraActivity.class.getSimpleName();
    Double lat;
    Double lng;




    @BindView(R.id.camera) CameraView camera;
    @BindView(R.id.button_flash) ImageButton imageButtonFlash;
    @BindView(R.id.button_picture) ImageButton imageButtonTakePicture;


    int i = 0;
    int[] drawables = new int[]{
                R.drawable.baseline_flash_off_white_36,
                R.drawable.baseline_flash_on_white_36,
                R.drawable.baseline_flash_auto_white_36,
                R.drawable.baseline_brightness_1_white_36};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_photo);
        ButterKnife.bind(this);
        imageButtonTakePicture.setVisibility(View.GONE);
        camera.addCameraKitListener(this);
        camera.setCropOutput(true);
        camera.setVideoQuality(CameraKit.Constants.VIDEO_QUALITY_480P);
        imageButtonFlash.setImageResource(drawables[i]);
        requestPermissions(Manifest.permission.ACCESS_FINE_LOCATION);
    }


    @OnClick(R.id.button_picture)
    public void takePhoto(){
        camera.captureImage();
        camera.setFlash(CameraKit.Constants.FLASH_OFF);

    }


    @Override
    protected void locationPermissionsGranted(boolean permissionsGranted) {
        super.locationPermissionsGranted(permissionsGranted);
        if(permissionsGranted){

            CustomLocationProvider.requestSingleUpdate(this, new CustomLocationProvider.LocationCallback() {
                @Override
                public void onNewLocationAvailable(Location location) {
                    imageButtonTakePicture.setVisibility(View.VISIBLE);
                    lat = location.getLatitude();
                    lng = location.getLongitude();
                }
            });

        }else{
            finish();
        }
    }

    @OnClick(R.id.button_flash)
    public void flash(){
        i++;
        i = i%4==0 ? 0 : i; // loop de 0 a 3
        imageButtonFlash.setImageResource(drawables[i]);
        camera.setFlash(i);
    }

    @Override
    public void onEvent(CameraKitEvent cameraKitEvent) {

    }

    @Override
    public void onError(CameraKitError cameraKitError) {

    }

    @Override
    public void onImage(CameraKitImage cameraKitImage) {

        Bitmap imageBitmap = cameraKitImage.getBitmap();
        byte[] bArray = ConvertUtil.bitmapToByteArray(imageBitmap, 1024, 60);

        Bundle bundle = new Bundle();
        bundle.putByteArray(IMAGEN,bArray);
        bundle.putDouble(LAT, lat);
        bundle.putDouble(LNG, lng);

        Intent returnIntent = new Intent();
        returnIntent.putExtras(bundle);

        setResult(RESULT_OK,returnIntent);
        finish();

    }

    @Override
    public void onVideo(CameraKitVideo cameraKitVideo) {

    }

    @Override
    protected void onResume() {
        super.onResume();
        camera.start();
    }

    @Override
    protected void onPause() {
        super.onPause();
        camera.stop();
        camera.setFlash(CameraKit.Constants.FLASH_OFF);
    }



}
