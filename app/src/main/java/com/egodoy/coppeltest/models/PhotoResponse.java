package com.egodoy.coppeltest.models;

/**
 * Created by edgargodoy on 03/09/18.
 */

public class PhotoResponse {
    String uri;

    public String getUri() {
        return uri;
    }

    public void setUri(String uri) {
        this.uri = uri;
    }
}
