package com.egodoy.coppeltest.utils;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.Base64;

import java.io.ByteArrayOutputStream;

/**
 * Created by edgargodoy on 03/09/18.
 */

public class ConvertUtil {

    /**
     *
     * @param originalbitmap
     * @param thumbnailHeight
     * @param quality
     * @return
     */
    public static byte[] bitmapToByteArray(Bitmap originalbitmap, int thumbnailHeight, int quality) {
        Float width  = new Float(originalbitmap.getWidth());
        Float height = new Float(originalbitmap.getHeight());
        Float ratio = width/height;
        Bitmap imageBitmap = Bitmap.createScaledBitmap(originalbitmap, (int)(thumbnailHeight*ratio), thumbnailHeight, false);
        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        imageBitmap.compress(Bitmap.CompressFormat.JPEG, quality, bos);
        return bos.toByteArray();
    }

    /**
     *
     * @param bArray
     * @return
     */
    public static String byteArrayToBase64(byte[] bArray){
        return Base64.encodeToString(bArray,Base64.DEFAULT);
    }

    /**
     *
     * @param encodedImage
     * @return
     */
    public static Bitmap base64ToBitmap(String encodedImage){
        byte[] decodedString = Base64.decode(encodedImage, Base64.DEFAULT);
        return BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length);
    }

}
