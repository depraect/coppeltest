package com.egodoy.coppeltest.connection;

import android.util.Log;

import java.io.IOException;

import okhttp3.Interceptor;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;
import okio.Buffer;

/**
 * Created by edgargodoy on 03/09/18.
 */

public class LogInterceptor implements Interceptor {
    private static final String TAG = LogInterceptor.class.getSimpleName();

    @Override
    public Response intercept(Interceptor.Chain chain) throws IOException {


        Request request = chain.request();
        printRequest(request);

        long t1 = System.nanoTime();
        Log.d(TAG,String.format("Sending %s on %s%n%s",
                request.url(), chain.connection(), request.headers()));

        Response response = chain.proceed(request);

        long t2 = System.nanoTime();
        Log.d(TAG,String.format("Received response for %s in %.1fms%n",
                response.request().url(), (t2 - t1) / 1e6d));

        Log.d(TAG,"http code: "  +response.code()+"  "+response.message() );



        return response;
    }


    private void printRequest(Request request) {
        try {
            String requestJson = "";
            RequestBody copy = request.body();
            Buffer buffer = new Buffer();
            if (copy != null)
                copy.writeTo(buffer);
            requestJson = buffer.readUtf8();
            Log.d("REQUEST", "URL: " + request.url().toString() + " - Method: " + request.method().toString() + " - JSON: " + requestJson + " - HEADERS :" + request.headers().toString());
        } catch (final IOException e) {
        }
    }
}
