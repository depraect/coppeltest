package com.egodoy.coppeltest.connection;

import android.graphics.Bitmap;
import android.util.Log;

import com.egodoy.coppeltest.models.Photo;
import com.egodoy.coppeltest.utils.ConvertUtil;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.io.IOException;

import okhttp3.Interceptor;
import okhttp3.MediaType;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.ResponseBody;

/**
 * Created by edgargodoy on 04/09/18.
 */

public class PicassoInterceptor implements Interceptor {

    private static final  String TAG = PicassoInterceptor.class.getSimpleName();

    @Override
    public Response intercept(Chain chain) throws IOException {


        Log.d(TAG, "picasso request");
        Request request = chain.request();

        Log.d(TAG, request.url().toString());

        Response response = chain.proceed(request);
        String json  = response.body().source().readUtf8().toString();
        Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'").create();
        Photo photo = gson.fromJson(json, Photo.class);

        Log.d(TAG,"base64 " + photo.getBase64());

        Bitmap bitmap = ConvertUtil.base64ToBitmap(photo.getBase64());
        byte[] byteArray = ConvertUtil.bitmapToByteArray(bitmap,1024,100);

        MediaType contentType = response.body().contentType();
        ResponseBody body = ResponseBody.create(contentType, byteArray);
        return response.newBuilder().body(body).build();

    }
}
