package com.egodoy.coppeltest.connection;

import com.egodoy.coppeltest.models.Photo;
import com.egodoy.coppeltest.utils.ConvertUtil;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.util.List;
import java.util.concurrent.TimeUnit;
import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.functions.Function;
import io.reactivex.schedulers.Schedulers;
import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Path;

/**
 * Created by edgargodoy on 03/09/18.
 */

public class MyJsonApi {

    private IRetofit retrofitInstance;
    //private final static  String URL_ENDPOINT =  "https://api.myjson.com/";
    private final static  String URL_ENDPOINT =  "http://192.168.0.10:8080/api/";
    private static MyJsonApi instance;


    public static MyJsonApi getInstance() {
        if (instance==null){
            instance = new MyJsonApi();
        }
        return instance;
    }


    private interface IRetofit{
        @POST("photos")
        Observable<Photo> savePhoto(@Body Photo photo);

        @GET("photos/{id}")
        Observable<Photo> getPhoto(@Path("id") String idPhoto);

        @GET("photos")
        Observable<List<Photo>> getPhotoList();
    }



    private MyJsonApi() {

        Gson gson = new GsonBuilder()
                .serializeNulls()
                .setDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'")
                .create();

        OkHttpClient client = new OkHttpClient.Builder()
                .readTimeout(60, TimeUnit.SECONDS)
                .connectTimeout(60, TimeUnit.SECONDS)
                .addInterceptor(new LogInterceptor())
                .build();

        Retrofit retrofit = new Retrofit.Builder()
                .client(client)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .baseUrl(URL_ENDPOINT)
                .build();

        retrofitInstance = retrofit.create(IRetofit.class);
    }

    public Observable<Photo> savePhoto(Photo photo){

        return retrofitInstance.savePhoto(photo)
                .subscribeOn(Schedulers.newThread())
                .map(new Function<Photo, Photo>() {
                    @Override
                    public Photo apply(Photo photo) throws Exception {
                        return photo;
                    }
                })
                .observeOn(AndroidSchedulers.mainThread());

    }

    public Observable<Photo> getPhoto(String photoId){
        return retrofitInstance.getPhoto(photoId)
                .subscribeOn(Schedulers.newThread())
                .map(new Function<Photo, Photo>() {
                    @Override
                    public Photo apply(Photo photo) throws Exception {
                        photo.setImage(ConvertUtil.base64ToBitmap(photo.getBase64()));
                        return photo;
                    }
                })
                .observeOn(AndroidSchedulers.mainThread());
    }

    public Observable<List<Photo>> getPhotoList(){
        return retrofitInstance.getPhotoList()
                .subscribeOn(Schedulers.newThread())
                .map(new Function<List<Photo>, List<Photo>>() {
                    @Override
                    public List<Photo> apply(List<Photo> photos) throws Exception {
                        for (Photo photo : photos){
                            photo.setUrl(URL_ENDPOINT + "photos/" + photo.getId());
                        }
                        return photos;
                    }
                })
                .observeOn(AndroidSchedulers.mainThread());


    }



}
