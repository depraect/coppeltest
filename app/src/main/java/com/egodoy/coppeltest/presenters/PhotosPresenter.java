package com.egodoy.coppeltest.presenters;
import com.egodoy.coppeltest.connection.MyJsonApi;
import com.egodoy.coppeltest.models.Photo;
import java.util.List;

import io.reactivex.observers.ResourceObserver;

/**
 * Created by edgargodoy on 03/09/18.
 */

public class PhotosPresenter {

    private static final String TAG = PhotosPresenter.class.getSimpleName();
    IPhotoAdapterCallback listener;

    public interface IPhotoAdapterCallback{
        public void onGetPhotoListSuccess(List<Photo> photos);
        public void onGetPhotoListError(Throwable e);
    }


    public PhotosPresenter(IPhotoAdapterCallback callback) {
        this.listener = callback;
        MyJsonApi.getInstance()
                .getPhotoList()
                .subscribe(new ResourceObserver<List<Photo>>() {
                    @Override
                    public void onNext(List<Photo> value) {
                        listener.onGetPhotoListSuccess(value);
                    }

                    @Override
                    public void onError(Throwable e) {
                        listener.onGetPhotoListError(e);
                    }

                    @Override
                    public void onComplete() {

                    }
                });
    }




}
