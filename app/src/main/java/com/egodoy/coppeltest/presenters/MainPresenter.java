package com.egodoy.coppeltest.presenters;

import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.Log;

import com.egodoy.coppeltest.activities.CameraActivity;
import com.egodoy.coppeltest.application.App;
import com.egodoy.coppeltest.application.Constants;
import com.egodoy.coppeltest.connection.MyJsonApi;
import com.egodoy.coppeltest.models.Photo;
import com.egodoy.coppeltest.utils.ConvertUtil;

import java.util.HashSet;
import java.util.Set;

import io.reactivex.observers.ResourceObserver;


/**
 * Created by edgargodoy on 03/09/18.
 */

public class MainPresenter {

    private static final String TAG = MainPresenter.class.getSimpleName();


    IMainPresenterCallBack listener;

    public interface IMainPresenterCallBack{
        public void onSavePhotoSuccess(Photo photo);
        public void onSavePhotoError(Throwable e);
    }



    public MainPresenter(IMainPresenterCallBack listener) {
        this.listener = listener;
    }


    public void savePhotoBundle(Bundle bundle){

        byte[] blob = bundle.getByteArray(CameraActivity.IMAGEN);

        Double lat = bundle.getDouble(CameraActivity.LAT);
        Double lng = bundle.getDouble(CameraActivity.LNG);
        String base64 = ConvertUtil.byteArrayToBase64(blob);

        Photo photo = new Photo();
        photo.setBase64(base64);
        photo.setLat(lat);
        photo.setLng(lng);

        MyJsonApi.getInstance()
                .savePhoto(photo)
                .subscribe(new ResourceObserver<Photo>() {
                    @Override
                    public void onNext(Photo photo) {

                        listener.onSavePhotoSuccess(photo);
                    }

                    @Override
                    public void onError(Throwable e) {
                        listener.onSavePhotoError(e);
                    }

                    @Override
                    public void onComplete() {

                    }
                });

    }


}
