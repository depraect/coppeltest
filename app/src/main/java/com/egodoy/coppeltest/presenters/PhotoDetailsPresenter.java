package com.egodoy.coppeltest.presenters;

import com.egodoy.coppeltest.connection.MyJsonApi;
import com.egodoy.coppeltest.models.Photo;

import io.reactivex.observers.ResourceObserver;

/**
 * Created by edgargodoy on 03/09/18.
 */

public class PhotoDetailsPresenter {

    PhotoDetailsPresenterCallback listener;

    public interface PhotoDetailsPresenterCallback{
        public void onGetPhotoSuccess(Photo photo);
        public void onGetPhotoError(Throwable e);
    }

    public PhotoDetailsPresenter(PhotoDetailsPresenterCallback listener) {
        this.listener =listener;
    }

    public void getPhoto(String id){
        MyJsonApi.getInstance()
                .getPhoto(id)
                .subscribe(new ResourceObserver<Photo>() {
                    @Override
                    public void onNext(Photo photo) {
                        listener.onGetPhotoSuccess(photo);
                    }

                    @Override
                    public void onError(Throwable e) {
                        listener.onGetPhotoError(e);
                    }

                    @Override
                    public void onComplete() {

                    }
                });
    }


}
