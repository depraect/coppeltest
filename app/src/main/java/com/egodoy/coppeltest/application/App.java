package com.egodoy.coppeltest.application;

import android.app.Application;
import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.util.Log;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * Created by edgargodoy on 03/09/18.
 */

public class App extends Application {

    private static final String TAG = App.class.getSimpleName();
    private static App shareInstance;
    private SharedPreferences sharedPreferences;

    @Override
    public void onCreate() {
        super.onCreate();
        shareInstance = this;
        sharedPreferences = getSharedPreferences(App.class.getName(), Context.MODE_PRIVATE);
    }


    public static synchronized App getInstance(){
        return shareInstance;
    }


    public List<String> getUris(){
        Set<String> uris = sharedPreferences.getStringSet(Constants.KEY_URIS,null);
        if(uris==null){
            uris = new HashSet<String>();
        }

        for (String s: uris){
            Log.d(TAG,"uri 1 -> " + s);
        }

        return new ArrayList<String>(uris);
    }

    public void addUri(String uri) {
        List<String> list = getUris();
        list.add(uri);
        Set<String> set = new HashSet<String>(list);
        sharedPreferences.edit().putStringSet(Constants.KEY_URIS, set).apply();

        List<String> stringList = getUris();

        for (String s: stringList){
            Log.d(TAG,"uri-> " + s);
        }

    }

}
