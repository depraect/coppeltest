package com.egodoy.coppeltest.adapters;

import android.content.Context;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import com.egodoy.coppeltest.R;
import com.egodoy.coppeltest.connection.PicassoInterceptor;

import com.egodoy.coppeltest.models.Photo;
import com.squareup.picasso.OkHttp3Downloader;
import com.squareup.picasso.Picasso;



import java.util.List;
import okhttp3.OkHttpClient;



/**
 * Created by edgargodoy on 03/09/18.
 */

public class PhotoAdapter extends RecyclerView.Adapter<PhotoAdapter.PhotoViewHolder> {

    List<Photo> photoList;
    OnPhotoClickListener listener;
    Picasso picasso;
    OkHttp3Downloader downloader;




    public interface OnPhotoClickListener{
        public void OnPhotoClickListenerTouch(String uri);
    }


    public PhotoAdapter(List<Photo> photos, OnPhotoClickListener listener) {
        photoList = photos;
        this.listener = listener;

        OkHttpClient okHttpClient = new OkHttpClient.Builder()
                .addInterceptor(new PicassoInterceptor())
                .build();

        downloader = new OkHttp3Downloader(okHttpClient);

    }

    @NonNull
    @Override
    public PhotoViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        Context context = parent.getContext();
        picasso = new Picasso.Builder(context).downloader(downloader).build();
        View view = LayoutInflater.from(context).inflate(R.layout.photo_item, parent, false);
        return new PhotoViewHolder(view);
    }

    @Override
    public int getItemCount() {
        return photoList.size();
    }

    @Override
    public void onBindViewHolder(@NonNull final PhotoViewHolder holder, int position) {

        final Photo photo = photoList.get(position);
        holder.textView.setText(photo.getUrl());


        picasso.load(photo.getUrl()).into(holder.imageView);

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                listener.OnPhotoClickListenerTouch(photo.getUrl());
            }
        });

    }

    public static class PhotoViewHolder extends RecyclerView.ViewHolder {
        TextView textView;
        ImageView imageView;
        public PhotoViewHolder(View view) {
            super(view);
            textView = (TextView) view.findViewById(R.id.text_view);
            imageView = (ImageView)view.findViewById(R.id.image_view);
        }
    }






}
